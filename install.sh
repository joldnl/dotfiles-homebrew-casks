#!/bin/bash

# ------------------------------------
# Install Caskroom Applications
# ------------------------------------

echo ""
echo "-------------------------------------------"
echo "| Install Cask Applications ...           |"
echo "-------------------------------------------"

# Set target application folder
# echo "Set default application folder to /Applications ..."
# cask_args appdir: '/Applications'
# echo "Done! ..."
echo ""

# Install cask applications formulae
echo "Install the actual applications ..."
input="casks.txt"
while IFS= read -r var
do
    brew cask install $var
done < "$input"
echo "Done!"
echo ""
